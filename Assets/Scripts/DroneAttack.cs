﻿using UnityEngine;
using System.Collections;

public class DroneAttack : MonoBehaviour
{
    GameObject missilePfb;
    public GameObject leftMissile, rightMissile;
    bool isLeftDone,isRightDone;
    void Start()
    {
        isLeftDone = false;
        isRightDone = false;
        missilePfb = Resources.Load<GameObject>("Prefabs/MissilePfb");
    }
    void Update()
    {
        MissileAttack();
    }
    void MissileAttack()
    {
        if (!isLeftDone&&Input.GetAxis("Fire1") > 0)
        {
            isLeftDone = true;
            leftMissile.SendMessage("Attack");
        }
        if (!isRightDone&&Input.GetAxis("Fire2") > 0)
        {
            isRightDone = true;
            rightMissile.SendMessage("Attack");
        }
    }
}
