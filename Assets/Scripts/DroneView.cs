﻿using UnityEngine;
using System.Collections;

public class DroneView : MonoBehaviour {

    public Transform camera;
    [Range(0, 20)]
    public float viewSpeed = 20;
	void Update () {
        View();
    }

    void View()
    {
        float rightX = Input.GetAxis("Right Horizontal");
        float rightY = Input.GetAxis("Right Vertical");
        //height up/down
        camera.RotateAround(camera.parent.position, transform.up, rightX * Time.deltaTime * viewSpeed);
        if (( camera.localEulerAngles.x > 330 && camera.localEulerAngles.x <= 360)
            || (camera.localEulerAngles.x < 75&& camera.localEulerAngles.x >= 0)
            || (camera.localEulerAngles.x < 330&& camera.localEulerAngles.x > 300 && rightY < 0)
            || (camera.localEulerAngles.x > 75 && camera.localEulerAngles.x < 90 && rightY > 0))
            camera.Rotate((-Vector3.right * rightY) * Time.deltaTime * viewSpeed, Space.Self);
    }

}
