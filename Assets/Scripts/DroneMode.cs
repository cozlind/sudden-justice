﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DroneMode : MonoBehaviour {

    public int currentIndex = 0;
    public GameObject[] modes;
    public List<GameObject> openModeList;
    void Start()
    {
        foreach(GameObject mode in modes)
        {
            mode.SetActive(false);
        }
        modes[currentIndex].SetActive(true);
        openModeList = new List<GameObject>();
        openModeList.Add(modes[currentIndex]);
    }
    void Update()
    {
        Switch();
    }
    bool keyLock = false;
    void Switch()
    {
        if (Input.anyKeyDown)
        {
            keyLock = true;
            float menuX = Input.GetAxisRaw("Menu Switch");
            currentIndex = (currentIndex + Mathf.RoundToInt(menuX)+ modes.Length) % modes.Length;
            ResetUI();
            modes[currentIndex].SetActive(true);
            openModeList.Add(modes[currentIndex]);
        }
    }
    void ResetUI()
    {
        foreach(GameObject openMode in openModeList)
        {
            openMode.SetActive(false);
        }
        openModeList.Clear();
    }
}
