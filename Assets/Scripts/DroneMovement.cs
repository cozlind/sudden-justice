﻿using UnityEngine;
using System.Collections;

public class DroneMovement : MonoBehaviour {
    [Range(0,50)]
    public float rotateSpeed=10;
    [Range(0, 100)]
    public float moveSpeed=10;
    [Range(0,0.05f)]
     public float joltSpeed = 0.05f;
    [Range(0, 0.01f)]
    public float joltAmplification = 0.005f;

    static Rigidbody rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    void Update()
    {
        Move();
        Jolt();
    }

    void Move()
    {
        float leftX = Input.GetAxis("Left Horizontal");
        float leftY = Input.GetAxis("Left Vertical");
        transform.Rotate((-Vector3.right * leftY - Vector3.forward * leftX + Vector3.up * leftX) * Time.deltaTime * rotateSpeed);
        Vector3 moveVector = Vector3.forward  * Time.deltaTime * moveSpeed;
        transform.Translate(moveVector);
    }
    void Jolt()
    {
        Random.InitState(Mathf.RoundToInt(Time.time));
        float rand=Random.Range(-0.001f, 0.001f);


        //float noise = Mathf.PerlinNoise(Time.time, Time.time);
        //noise = (noise * 2 - 1f)* jolt;
        float offsetX = (rand + joltAmplification) * Mathf.Sin(Random.Range(-1, 1)*joltSpeed * Vector2.Dot(new Vector2(Time.time , 0), new Vector2(18,25)));
        float offsetY = (rand + joltAmplification) * Mathf.Sin(joltSpeed * Vector2.Dot(new Vector2( Time.time + 23f,0), new Vector2(18, 25))) ;


        Vector3 offset = Vector3.right * offsetX + Vector3.up * offsetY;
        transform.Translate(offset);
    }
}
