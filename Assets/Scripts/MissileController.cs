﻿using UnityEngine;
using System.Collections;

public class MissileController : MonoBehaviour {
    [Range(0,1000)]
    public float speed = 200;
    GameObject flare;
    GameObject explosion;
    Rigidbody rigidbody;
	void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        flare = transform.GetChild(0).gameObject;
        explosion = transform.GetChild(1).gameObject;

        flare.SetActive(false);
        explosion.SetActive(false);
    }

    Ray ray;
    void Update()
    {
        ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width/2,Screen.height/2,0));
        Debug.DrawRay(ray.origin, ray.direction * 1000);
    }



    void Attack()
    {
        rigidbody.useGravity = true;
        flare.SetActive(true);
        rigidbody.velocity = ray.direction * speed;// transform.forward * speed;

        Invoke("DestroySelf", 5);
    }
    void OnCollisionEnter(Collision collision)
    {
        explosion.SetActive(true);

        collision.rigidbody.AddExplosionForce(1000, collision.contacts[0].point, 20);

        Invoke("DestroySelf", 2);
    }



    void DestroySelf()
    {
        if (gameObject != null)
        {
            Destroy(gameObject);
        }
    }
}
